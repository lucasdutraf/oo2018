#ifndef TRAPEZIO_HPP
#define TRAPEZIO_HPP
#include "forma_geometrica.hpp"
using namespace std;

class Trapezio : public FormaGeometrica {

private:
    float base2;

public:
    Trapezio();
    Trapezio(float base1, float base2, float altura);
    ~Trapezio();

    void setBase2(float base2);
    float getBase2();

    float calculaArea();
    float calculaPerimetro();


};
#endif