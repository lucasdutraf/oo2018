#ifndef TRIANGULO_HPP
#define TRIANGULO_HPP
#include <string>
#include "forma_geometrica.hpp"
using namespace std;

class Triangulo : public FormaGeometrica {

    public:
        Triangulo();
        Triangulo(float base, float altura);
        ~Triangulo();

        float calculaArea();
        float calculaPerimetro();


};
#endif