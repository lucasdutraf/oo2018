#include "forma_geometrica.hpp"
#include <iostream>
#include "triangulo.hpp"
using namespace std;

int main(int argc, char ** argv){

	FormaGeometrica forma1;
	FormaGeometrica * forma2 = new FormaGeometrica(8.0, 4.5);

	Triangulo * triangulo1 = new Triangulo(7.0, 4.0);

	cout << forma1.getTipo() << " área: " << forma1.calculaArea() << endl;
	cout << forma2->getTipo() << " área: " << forma2->calculaArea() << endl;
	cout << "triangulo" << triangulo1->calculaPerimetro() << endl;

	delete forma2;	
	delete triangulo1;
return 0;
}
