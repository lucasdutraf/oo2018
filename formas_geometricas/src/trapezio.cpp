#include "trapezio.hpp"
#include <iostream>
using namespace std;

Trapezio::Trapezio(){
    setBase(10.0);
    setAltura(15.0);
    setBase2(5.0);
    setTipo("Trapezio");
}
Trapezio::Trapezio(float base1, float base2, float altura){
    setBase(base1);
    setAltura(altura);
    setBase2(base2);
}
Trapezio::~Trapezio(){
    cout << "Destruindo a classe Trapezio" << endl;
}