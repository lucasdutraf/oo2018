#include "triangulo.hpp"
#include <math.h>
#include <iostream>
#include "forma_geometrica.hpp"
using namespace std;

Triangulo::Triangulo(){
    setBase(7.0);
    setAltura(4.0);
    setTipo("Triangulo");
}
Triangulo::Triangulo(float base, float altura){
    setBase(base);
    setAltura(altura);
    setTipo("Triangulo");
}
Triangulo::~Triangulo(){
    cout << "Destrutor do triângulo" << endl;
}

float Triangulo::calculaArea(){
    return (getBase() * getAltura())/2;
}
float Triangulo::calculaPerimetro(){
    float h;
    h = sqrt(pow(getBase(), 2) + pow(getAltura(), 2));
    return h + getBase() + getAltura();
}

