#include <iostream>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"
#include <string>
using namespace std;

int main(int argc, char  ** argv){

	Aluno aluno1;
	Professor professor1;

	aluno1.setNome("João");
	aluno1.setMatricula(4588);

	professor1.setNome("Maria");
	professor1.setMatricula(1801564);

	cout << aluno1.getNome() <<endl;
	cout << aluno1.getMatricula() << endl;

	cout << professor1.getNome() << endl;
	cout << professor1.getMatricula() << endl;


return 0;
}
