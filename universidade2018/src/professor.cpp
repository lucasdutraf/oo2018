#include "professor.hpp"
#include <string>
#include <iostream>
using namespace std;

Professor :: Professor(){
	disciplinas_lecionadas = "";
	salario = 10000.50;
	sala = "i6";
	turmas = "A";
	cout << "Professor criado" << endl;
}

void Professor :: setDisciplinas_lecionadas(string disciplinas_lecionadas){
	this->disciplinas_lecionadas = disciplinas_lecionadas;
}
string Professor :: getDisciplinas_lecionadas(){
	return disciplinas_lecionadas;
}

void Professor :: setSalario(float salario){
	this->salario = salario;
}
float Professor :: getSalario(){
	return salario;
}

void Professor :: setSala(string sala){
	this->sala = sala;
}
string Professor :: getSala(){
	return sala;
}

void Professor :: setTurmas(string turmas){
	this->turmas = turmas;
}
string Professor :: getTurmas(){
	return turmas;
}

Professor :: ~Professor(){
	cout << "Professor destruído" << endl;
}



