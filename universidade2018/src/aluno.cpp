#include <iostream>
#include "aluno.hpp"
using namespace std;


	Aluno :: Aluno(){
	
	creditos = 10;
	ira = 5.0;
	semestre = 2;
	curso = "engenharia_de_software";
	cout << "Aluno criado" << endl; 
}


	void Aluno ::setCreditos(int creditos){
		this->creditos = creditos;
	}
	int Aluno ::getCreditos(){
		return creditos;
	}

	void Aluno ::setIra(float ira){
			this->ira = ira;
	}
	float Aluno ::getIra(){
		return ira;
	}

	void Aluno ::setSemestre(int semestre){
			this->semestre = semestre;
	}
	int Aluno ::getSemestre(){
		return semestre;
	}

	void Aluno ::setCurso(string curso){
			this->curso = curso;
	}
	string Aluno ::getCurso(){
		return curso;
	}
	
	void Aluno :: imprimeDados(){
	
	cout << "Quantidade de créditos obtida: " << getCreditos() << endl;
	
	cout <<"Índice de rendimento acadêmico: " <<  getIra() << endl;
	
	cout <<"Semestres cursados" <<  getSemestre() << endl;
	
	cout << "Curso atual: " <<  getCurso() << endl;
	}
	
Aluno :: ~Aluno(){
	cout <<"Aluno destruído" << endl;
}
