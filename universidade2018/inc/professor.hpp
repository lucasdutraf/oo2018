#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP
#include <string>
#include "pessoa.hpp"

class Professor : public Pessoa{
//atributos
private:
	string disciplinas_lecionadas;
	float salario;
	string sala;
	string turmas;

//Métodos
public:
	Professor();
	~Professor();
	
	void setDisciplinas_lecionadas(string disciplinas_lecionadas);
	string getDisciplinas_lecionadas();

	void setSalario(float salario);
	float getSalario();
	
	void setSala(string sala);
	string getSala();
	
	void setTurmas(string turmas);
	string getTurmas();

};
#endif


