#ifndef COMPLEXO_HPP
#define COMPLEXO_HPP

using namespace std;

class Complexo {
private:
	float real;
	float imaginario;
public:
	Complexo() : real(0), imaginario(0){};
	Complexo(float real, float imaginario) : real(real), imaginario(imaginario){};
	~Complexo();

	void entrada();
	void imprime();

	Complexo operator + (Complexo c2);
	Complexo operator - (Complexo c2);
	Complexo operator * (Complexo c2);
};
#endif

