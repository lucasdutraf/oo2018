#include "complexo.hpp"
#include <iostream>
using namespace std;

void Complexo::entrada(){
	cout << "(Real Imaginario): ";
	cin >> real >> imaginario;
} 

void Complexo::imprime(){
	if(imaginario < 0)
		cout << real << imaginario << "i" << endl;
	else
		cout << real << "+" << imaginario << "i" <<  endl;

}
Complexo :: ~Complexo(){}

Complexo Complexo::operator + (Complexo c2){
	Complexo resultado;
	resultado.real = real + c2.real;
	resultado.imaginario = imaginario + c2.imaginario;
	return resultado;
}

Complexo Complexo::operator - (Complexo c2){
	Complexo resultado;
	resultado.real = real - c2.real;
	resultado.imaginario = imaginario - c2.imaginario;
	return resultado;
}

Complexo Complexo::operator * (Complexo c2){
	Complexo resultado;
	resultado.real = (real*c2.real - imaginario*c2.imaginario);
	resultado.imaginario = (real*c2.imaginario + imaginario*c2.real);
	return resultado;
}
